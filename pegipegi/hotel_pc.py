import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class HotelPc(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def search(self):
        self.url = 'http://www.pegipegi.com/'
        self.keyword = 'maven'

    def test_search(self):
        self.search()
        self.driver.get("http://www.pegipegi.com/")
        elem = self.driver.find_element_by_name('hotelNameKey')
        elem.send_keys(self.keyword)
        elem.send_keys(Keys.RETURN)
        self.assertTrue("Maaf tidak ada hasil." not in self.driver.page_source)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main(warnings='ignore')
