SELENEST
=========

Selenest is script for automated unit testing to aid exploratory testing.

> * Beautiful is better than ugly.
> * Explicit is better than implicit.
> * Simple is better than complex.
> * Complex is better than complicated.
> * Readability counts.
> * Practicality beat purity.
>
> -- The zen of python --


### Get Started ###
* current version is 0.1.0

Selenest is written in python with platform independence in mind and run under selenium. as a result Selenest is available to automation test web apps written in any programming language.

### Requirements ###
* Python 3.3/3.4
* Python Selenium 2.44.0 or more

### Contributors ###
* pseudecoder
